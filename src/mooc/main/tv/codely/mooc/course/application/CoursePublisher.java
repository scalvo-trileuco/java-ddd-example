package tv.codely.mooc.course.application;

import tv.codely.mooc.course.domain.Course;
import tv.codely.mooc.course.domain.CourseDescription;
import tv.codely.mooc.course.domain.CourseTitle;
import tv.codely.shared.domain.EventBus;

public final class CoursePublisher {
    private final EventBus eventBus;

    public CoursePublisher(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void publish(String rawTitle, String rawDescription) {
        final var title = new CourseTitle(rawTitle);
        final var description = new CourseDescription(rawDescription);

        final var course = Course.publish(title, description);

        eventBus.publish(course.pullDomainEvents());
    }
}


package tv.codely.mooc.course.domain;

import tv.codely.mooc.video.domain.VideoPublished;
import tv.codely.shared.domain.AggregateRoot;

public final class Course  extends AggregateRoot {
    private final CourseTitle title;
    private final CourseDescription description;

    private Course(CourseTitle title, CourseDescription description) {
        this.title = title;
        this.description = description;
    }

    public static Course publish(CourseTitle title, CourseDescription description) {
        var course = new Course(title, description);

        var courseCreated = new CoursePublished(title.value(), description.value());

        course.record(courseCreated);

        return course;
    }
}


package tv.codely.mooc.course.infrastructure;

import tv.codely.mooc.course.application.CoursePublisher;
import tv.codely.mooc.notification.application.create.SendPushToSubscribersOnCoursePublished;
import tv.codely.shared.application.DomainEventSubscriber;
import tv.codely.shared.domain.EventBus;
import tv.codely.shared.infrastructure.bus.ReactorEventBus;

import java.util.Set;

public class CoursePublisherCliController {
    public static void main(String[] args) {
        final Set<DomainEventSubscriber> subscribers = Set.of(
                new SendPushToSubscribersOnCoursePublished()
        );
        final EventBus eventBus = new ReactorEventBus(subscribers);
        final var coursePublisher = new CoursePublisher(eventBus);

        final var courseTitle = "\uD83C\uDF89 New YouTube.com/CodelyTV course title";
        final var courseDescription = "This should be the course description \uD83D\uDE42";

        coursePublisher.publish(courseTitle, courseDescription);
    }
}

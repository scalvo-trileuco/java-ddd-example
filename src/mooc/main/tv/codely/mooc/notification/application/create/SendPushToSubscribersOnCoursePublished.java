package tv.codely.mooc.notification.application.create;

import tv.codely.mooc.course.domain.CoursePublished;
import tv.codely.shared.application.DomainEventSubscriber;

public class SendPushToSubscribersOnCoursePublished implements DomainEventSubscriber<CoursePublished> {
    @Override
    public Class<CoursePublished> subscribedTo() {
        return CoursePublished.class;
    }

    @Override
    public void consume(CoursePublished event) {
        System.out.println(
            String.format(
                "Hey! There is a new course with title <%s> and description <%s>",
                event.title(),
                event.description()
            )
        );
    }
}

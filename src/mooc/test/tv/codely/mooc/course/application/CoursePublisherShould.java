package tv.codely.mooc.course.application;

import org.junit.jupiter.api.Test;
import tv.codely.mooc.course.domain.CoursePublished;
import tv.codely.shared.domain.EventBus;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

final class CoursePublisherShould {
    @Test
    void publish_the_course_published_domain_event() {
        final EventBus eventBus = mock(EventBus.class);
        final var coursePublished = new CoursePublisher(eventBus);

        final var courseTitle = "\uD83C\uDF89 New YouTube.com/CodelyTV course title";
        final var courseDescription = "This should be the course description \uD83D\uDE42";

        coursePublished.publish(courseTitle, courseDescription);

        final var expectedCourseCreated = new CoursePublished(courseTitle, courseDescription);

        verify(eventBus).publish(List.of(expectedCourseCreated));
    }

}
